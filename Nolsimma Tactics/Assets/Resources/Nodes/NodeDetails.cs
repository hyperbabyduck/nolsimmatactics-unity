﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Node", menuName = "Nodes/Create New Node", order = 1)]
public class NodeDetails : ScriptableObject
{
	[SerializeField] Sprite sprite;
	[SerializeField] bool passable;
	[SerializeField] float movementCost;

	public Sprite GetSprite()
	{
		return sprite;
	}

	public bool GetPassable()
	{
		return passable;
	}

	public float GetMovementCost()
	{
		return movementCost;
	}
}
