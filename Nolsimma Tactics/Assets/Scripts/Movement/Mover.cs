﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DirectionFacing
{
    Right,
    Up,
    Left,
    Down
}

public class Mover : MonoBehaviour
{
    [SerializeField] float moveSpeed = 1f;

    IEnumerator movementCoroutine;

    float storedMoveSpeed = 0f;

    private void Start()
    {
        storedMoveSpeed = moveSpeed;
    }

    public void SetIsPaused(bool value)
    {
        if (value)
        {
            moveSpeed = 0f;
        }
        else
        {
            moveSpeed = storedMoveSpeed;
        }
    }

    public IEnumerator FollowPath(List<Node> path, Unit unit)
    {
        GridController gridController = GameObject.FindGameObjectWithTag("GridController").GetComponent<GridController>();
        unit.SetIsMoving(true);
        gridController.EmptyGridPos(unit.GridPos);

        //go through waypoints in order
        foreach (Node node in path.ToArray())
        {
            movementCoroutine = MoveToNode(node, unit);

            yield return movementCoroutine;

            //set unit's grid position 
            unit.GridPos = node.GridPos;
        }

        gridController.WipePath();
        gridController.OccupyGridPos(unit, unit.GridPos);

        unit.SetIsMoving(false);
        unit.EndTurn();
    }

    private IEnumerator MoveToNode(Node targetNode, Unit unit)
    {
        DirectionFacing newDirection = FindDirection(unit, targetNode);

        unit.SetDirection(newDirection);

        //if the player isnt where it should be
        while (transform.position != targetNode.transform.position)
        {
            //move towards the position
            transform.position = Vector2.MoveTowards(transform.position, targetNode.transform.position, moveSpeed * Time.deltaTime);

            yield return null;
        }
    }

    private DirectionFacing FindDirection(Unit unit, Node targetNode)
    {
        DirectionFacing direction = DirectionFacing.Down;

        Vector2Int gridPos = unit.GridPos;
        Vector2Int targetGridPos = targetNode.GridPos;

        //right +1, 0
        if (gridPos.x < targetGridPos.x && gridPos.y == targetGridPos.y)
        {
            direction = DirectionFacing.Right;
        }
        //up 0, -1
        else if (gridPos.x == targetGridPos.x && gridPos.y > targetGridPos.y)
        {
            direction = DirectionFacing.Up;
        }
        //left -1, 0
        else if (gridPos.x > targetGridPos.x && gridPos.y == targetGridPos.y)
        {
            direction = DirectionFacing.Left;
        }
        //down 0, +1
        else if (gridPos.x == targetGridPos.x && gridPos.y < targetGridPos.y)
        {
            direction = DirectionFacing.Down;
        }
        return direction;
    }
}
