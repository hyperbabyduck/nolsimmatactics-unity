﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayCurrentUnit : MonoBehaviour
{
    [SerializeField] Text unitText;

    public void SetText(string value)
    {
        unitText.text = value;
    }
}
