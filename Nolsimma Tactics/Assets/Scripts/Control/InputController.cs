﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public enum InputState
{
    None,
    Action,
    Move,
}

public class InputController : MonoBehaviour
{ 
	InputState inputState = InputState.None;

	[SerializeField] GameObject inputButtons;

    public InputState InputMode
    {
        get { return inputState; }
        set { inputState = value; }
    }

    public void SetInputButtons(bool value)
    {
        inputButtons.SetActive(value);
    }
}
