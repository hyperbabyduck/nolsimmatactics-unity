﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Globals
{
    public const float TILE_WIDTH_HALF = .5f;
    public const float TILE_HEIGHT_HALF = .25f;
}

public enum NodeColour
{
    Move,
    Range,
    None
}

[System.Serializable]
public class NodeColours
{
    public Color moveNodeColour;
    public Color selectedMoveNodeColour;
    public Color rangeNodeColour;
    public Color selectedRangeNodeColour;
    public Color blueOccupiedNodeColour;
    public Color redOccupiedNodeColour;
    public Color noTeamOccupiedNodeColour;
}

public class GridController : MonoBehaviour
{
    [SerializeField] NodeColours nodeColours;
    Dictionary<Vector2Int, Node> grid = new Dictionary<Vector2Int, Node>();
    Queue<Node> queue = new Queue<Node>();

    Node currentNode = null;
    List<Node> path = new List<Node>();

    Vector2Int[] directions =
    {
        Vector2Int.right,
        Vector2Int.up,
        Vector2Int.left,
        Vector2Int.down
    };

    //SETUP
    public void CreateGrid()
    {
        //find all of the Nodes
        Node[] nodes = FindObjectsOfType<Node>();

        //add their position to the dictionary
        foreach (Node node in nodes)
        {
            var gridPos = node.AttachToGrid();

            //if there are two Nodes at the same coordinates, skip the second one
            if (grid.ContainsKey(gridPos))
            {
                Debug.Log("Skipping overlapping block : " + node);
            }
            else
            {
                //add to grid
                grid.Add(gridPos, node);

                //set colours
                node.SetColourValues(nodeColours);                
            }
        }

        AdjustElevationLevels(nodes);
    }

    private void AdjustElevationLevels(Node[] nodes)
    {
        foreach (Node node in nodes)
        {
            node.ChangeElevation();
        }
    }

    //MOVE RANGE
    public void FindMoveNodes(Unit currentUnit)
    {
        print("Find move nodes");
        Node startingNode = grid[currentUnit.GridPos];

        //create path list
        queue = new Queue<Node>();

        BreadthFirstSearch(startingNode, currentUnit);

        //store start node for undo move purposes
        currentUnit.PreviousNode = startingNode;
        
        //set input mode to allow input
        GameObject.FindGameObjectWithTag("InputController").GetComponent<InputController>().InputMode = InputState.Move;
    }

    private void BreadthFirstSearch(Node startingNode, Unit currentUnit)
    { 
        //add the starting Node to the queue
        queue.Enqueue(startingNode);

        //while there are Nodes still to check and the end hasnt been found
        while (queue.Count > 0)
        {
            //remove current Node from queue
            currentNode = queue.Dequeue();

            //Add neighbours of the current Node to the queue
            ExploreNeighbours(currentUnit);

            //set the current waypooint as explored
            currentNode.IsExplored = true;

            //colour node
            currentNode.ColourNode();
        }
    }

    private void ExploreNeighbours(Unit currentUnit)
    {
        //search for a neighbour in all 4 directions
        foreach (Vector2Int direction in directions)
        {
            //find the current neighbour to be checked
            Vector2Int neighbourCoordinates = currentNode.GridPos + direction;

            //if the neighbour Node exists
            if (grid.ContainsKey(neighbourCoordinates))
            {
                //add it to the queue
                QueueNewNeighbours(neighbourCoordinates, currentUnit);
            }
        }
    }

    private void QueueNewNeighbours(Vector2Int neighbourCoordinates, Unit currentUnit)
    {
        //get the neighbour Node at the coordinates
        Node neighbour = grid[neighbourCoordinates];

        //if the neighbour hasnt already been searched and isnt already in the queue
        if (!neighbour.IsExplored || queue.Contains(neighbour))
        {
            //if the neighbour is passable and within the unit's move range
            if (neighbour.Passable && (currentNode.TotalMovementCost + neighbour.MovementCost) <= currentUnit.MoveRange)
            {
                //if the node isnt occupied, occupied by a knocked out unit or occupied by a teammate
                if (!neighbour.IsOccupied || !neighbour.Occupant.IsConscious || neighbour.Occupant.Team == currentUnit.Team)
                {
                    //add it to the queue and set where it was added from
                    queue.Enqueue(neighbour);
                    neighbour.ExploredFrom = currentNode;

                    //give the node a movement cost
                    neighbour.TotalMovementCost = currentNode.TotalMovementCost + neighbour.MovementCost;

                    neighbour.IsMoveNode = true;
                }
            }
        }
    }

    //PATHFINDING
    public List<Node> GetPath(Node startingNode, Node targetNode)
    {
        path = new List<Node>();

        //if the path is empty
        if (path.Count <= 0)
        {
            if (startingNode == targetNode)
            {
                path.Add(startingNode);
            }
            else
            {
                CreatePath(startingNode, targetNode);
            }
        }

        return path;
    }

    private void CreatePath(Node startingNode, Node targetNode)
    {
        //add the target Node to the queue
        SetAsPath(targetNode);

        //find the Node it was searched from
        Node previous = targetNode.ExploredFrom;

        //keep stepping back through the Nodes to teh start
        while (previous != startingNode)
        {
            SetAsPath(previous);
            previous = previous.ExploredFrom;
        }

        //add the starting position to the end of the queue
        SetAsPath(startingNode);

        //reverse the queue so it is in order
        path.Reverse();
    }

    private void SetAsPath(Node Node)
    {
        path.Add(Node);
    }

    public void WipePath()
    {
        path.Clear();
        queue.Clear();
    }

    //NODES
    public Node GetNodeFromGrid(Vector2Int gridPos)
    {
        return grid[gridPos];
    }

    public void WipeNodes()
    {
        Node[] nodes = FindObjectsOfType<Node>();

        foreach (Node node in nodes)
        {
            node.IsExplored = false;
            node.IsMoveNode = false;
            node.IsRangeNode = false;
            node.TotalMovementCost = 0;

            node.SetNodeColour(Color.white, false);
        }
    }

    //OCCUPATION
    public void OccupyGridPos(Unit unit, Vector2Int gridPos)
    {
        grid[gridPos].Occupant = unit;
        grid[gridPos].IsOccupied = true;
    }

    public void EmptyGridPos(Vector2Int gridPos)
    {
        grid[gridPos].Occupant = null;
        grid[gridPos].IsOccupied = false;
    }
}
