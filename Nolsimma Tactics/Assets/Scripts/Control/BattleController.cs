﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TeamColour
{
	Blue,
	Red,
	None
}

public class BattleController : MonoBehaviour
{
	[SerializeField] GridController gridController;
	[SerializeField] DisplayCurrentUnit currentUnitDisplay;

	Unit currentUnit;
	int roundNumber = 0;

	private void Start()
	{
		InitializeBattle();
	}

	public void InitializeBattle()
	{
		//create the grid
		gridController.CreateGrid();

		//Find the starting character
		currentUnit = GetComponent<UnitController>().FindStartingUnit();

		//Set Unit UI
		currentUnitDisplay.SetText(currentUnit.Name);

		currentUnit.StartTurn();
	}

	public void StartNextTurn()
	{
		//have the end game conditions been met?
		{
			currentUnit = GetComponent<UnitController>().GetNextUnit();
			if (GetComponent<UnitController>().CurrentUnitIndex ==  0)
			{
				RoundNumber += 1;
			}

			currentUnit.StartTurn();
		}
		//if the game have ended
		{

		}
	}

	public Unit SelectedUnit
	{
		get { return currentUnit; }
		set { currentUnit = value; }
	}

	public int RoundNumber
	{
		get { return roundNumber; }
		set { roundNumber = value; }
	}
}
