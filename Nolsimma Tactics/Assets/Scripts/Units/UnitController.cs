﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitController : MonoBehaviour
{
	List<Unit> unitList = new List<Unit>();

	int currentUnitIndex = 0;

	public Unit FindStartingUnit()
	{
		FillUnitList();
		return FindUnitInList(currentUnitIndex);
	}

	private void FillUnitList()
	{
		//Get all units in the scene
		Unit[] foundUnits = FindObjectsOfType<Unit>();

		//Add them all to list
		foreach (Unit unit in foundUnits)
		{
			unitList.Add(unit);

			//add unit to grid
			unit.AttachToGrid();
		}
	}

	private Unit FindUnitInList(int index)
	{
		//find the first unit in the units list
		return unitList[index];
	}

	public Unit GetNextUnit()
	{
		//check if next number is less than the total number of units
		if(currentUnitIndex + 1 < unitList.Count)
		{
			//increment number
			currentUnitIndex += 1;
		}
		else
		{
			//reset number
			currentUnitIndex = 0;
		}

		//return next unit
		return FindUnitInList(currentUnitIndex);
	}

	public int CurrentUnitIndex
	{
		get { return currentUnitIndex; }
		set { currentUnitIndex = value; }
	}
}
