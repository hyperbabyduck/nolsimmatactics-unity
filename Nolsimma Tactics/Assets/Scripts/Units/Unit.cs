﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit : MonoBehaviour
{
	[SerializeField] string unitName = "error";
	[SerializeField] float moveRange;
	[SerializeField] TeamColour team;
	[SerializeField] bool isConscious = true;

	int actions = 0;
	bool canMove = false;
	bool canAct = false;

	bool isMoving = false;
	DirectionFacing directionFacing = DirectionFacing.Down;
	Node previousNode;

	InputController inputController;
	Mover myMover;
	Animator myAnimator;

	Vector2Int gridPos = new Vector2Int(0, 0);

	private void Start()
	{
		inputController = GameObject.FindGameObjectWithTag("InputController").GetComponent<InputController>();
		myAnimator = GetComponent<Animator>();
		myMover = GetComponent<Mover>();
		SetAnimatorDirection();
	}

	//SETUP
	private void GetActions()
	{
		actions = 2;
		canMove = true;
		canAct = true;
	}

	public Vector2Int AttachToGrid()
	{
		gridPos = new Vector2Int(
			Mathf.RoundToInt(((transform.position.x * Globals.TILE_WIDTH_HALF) + transform.position.y) / Globals.TILE_WIDTH_HALF),
			Mathf.RoundToInt(transform.position.x - (transform.position.y * 2))
			);

		GridController grid = GameObject.FindGameObjectWithTag("GridController").GetComponent<GridController>();
		grid.OccupyGridPos(this, gridPos);

		return gridPos;
	}

	public Vector2Int GridPos
	{
		get { return gridPos; }
		set { gridPos = value; }
	}

	//TURN
	public void StartTurn()
	{
		//get actions
		GetActions();

		//TODO Move to button
		GameObject.FindGameObjectWithTag("GridController").GetComponent<GridController>().FindMoveNodes(this);

		//Show Action Buttons
		GameObject.FindGameObjectWithTag("InputController").GetComponent<InputController>().InputMode = InputState.Move;
	}

	public void EndTurn()
	{
		//remove previous node
		PreviousNode = null;

		GameObject.FindGameObjectWithTag("BattleController").GetComponent<BattleController>().StartNextTurn();
	}

	//STATS
	public string Name
	{
		get { return unitName; }
		set { unitName = value; }
	}

	public float MoveRange
	{
		get { return moveRange; }
		set { moveRange = value; }
	}

	public TeamColour Team
	{
		get { return team; }
		set { team = value; }
	}

	public bool IsConscious
	{
		get { return isConscious; }
		set { isConscious = value; }
	}

	//MOVEMENT
	public void StartMovement(Node targetNode)
	{
		if (!isMoving)
		{
			GridController grid = GameObject.FindGameObjectWithTag("GridController").GetComponent<GridController>();

			//Get the path from the path controller
			List<Node> path = grid.GetPath(grid.GetNodeFromGrid(gridPos), targetNode);

			grid.WipeNodes();

			//Send the path to my mover to begin movement
			StartCoroutine(myMover.FollowPath(path, this));

			isMoving = true;
		}
	}

	public bool IsMoving
	{
		get { return isMoving; }
		set
		{
			isMoving = value;

			if (isMoving == true)
			{
				myAnimator.SetTrigger("isWalking");
			}
			else
			{
				myAnimator.SetTrigger("isIdle");
			}
		}
	}

	public void SetIsMoving(bool value)
	{
		isMoving = value;

		if (isMoving == true)
		{
			myAnimator.SetTrigger("isWalking");
		}
		else
		{
			myAnimator.SetTrigger("isIdle");
		}
	}

	public Node PreviousNode
	{
		get { return previousNode; }
		set { previousNode = value; }
	}

	//ANIMATION
	private void SetAnimatorDirection()
	{
		myAnimator.SetInteger("directionFacing", (int)directionFacing);
	}

	public void SetDirection(DirectionFacing facing)
	{
		myAnimator.SetInteger("directionFacing", (int)facing);
		myAnimator.SetTrigger("isWalking");
	}

	public DirectionFacing GetCurrentDirection()
	{
		return directionFacing;
	}


}
