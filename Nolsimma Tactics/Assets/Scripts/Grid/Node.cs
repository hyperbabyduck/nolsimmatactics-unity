﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Node : MonoBehaviour
{
    [Header("Node Info")]
    [SerializeField] NodeDetails nodeDetails;
    [SerializeField] int elevationLevel = 0;
    [SerializeField] SpriteRenderer nodeSprite;

    [Header("Node UI")]
    [SerializeField] SpriteRenderer nodeOverlay;
    [SerializeField] Text gridNumberText;

    //Colours
    Color moveNodeColour;
    Color selectedMoveNodeColour;
    Color rangeNodeColour;
    Color selectedRangeNodeColour;
    Color blueOccupiedNodeColour;
    Color redOccupiedNodeColour;
    Color noTeamOccupiedNodeColour;

    Vector2Int gridPos = new Vector2Int(0, 0);
    InputController inputController;

    //Node Stats
    bool isExplored = false;
    Node exploredFrom = default;
    bool isOccupied = false;
    Unit occupant = null;
    bool isMoveNode = false;
    bool isRangeNode = false;

    float totalMovementCost = 0f;
    float movementCost = 0f;
    bool passable;

    private void Awake()
    {
        //LOAD STATS FROM SCRIPTABLEOBJECT
        nodeSprite.sprite = nodeDetails.GetSprite();
        Passable = nodeDetails.GetPassable();
        movementCost = nodeDetails.GetMovementCost();
    }

    private void Start()
    {
        inputController = GameObject.FindGameObjectWithTag("InputController").GetComponent<InputController>();
    }

	private void Update()
	{
        SetText(IsMoveNode.ToString());
    }

	//PATHFINDING
	public Vector2Int AttachToGrid()
    {
        gridPos = new Vector2Int(
            Mathf.RoundToInt(((transform.position.x * Globals.TILE_WIDTH_HALF) + transform.position.y) / Globals.TILE_WIDTH_HALF),
            Mathf.RoundToInt(transform.position.x - (transform.position.y * 2))
            );

        return gridPos;
    }

    public Vector2Int GridPos
    {
        get { return gridPos; }
        set { gridPos = value; }
    }

    public bool IsExplored
    {
        get{ return isExplored; }
        set{ isExplored = value; }
    }

    public Node ExploredFrom
    {
        get{ return exploredFrom; }
        set{ exploredFrom = value; }
    }

    //NODE INFO
    public bool IsOccupied
    {
        get{ return isOccupied; }
        set{ isOccupied = value; }
    }

    public Unit Occupant
    {
        get{ return occupant; }
        set{ occupant = value; }
    }

    public float TotalMovementCost
    {
        get { return totalMovementCost; }
        set { totalMovementCost = value; }
    }

    public float MovementCost
    {
        get { return movementCost; }
        set { movementCost = value; }
    }

    public bool Passable
    {
        get { return passable; }
        set { passable = value; }
    }

    public bool IsMoveNode
    {
        get { return isMoveNode; }
        set { isMoveNode = value; }
    }

    public bool IsRangeNode
    {
        get { return isRangeNode; }
        set { isRangeNode = value; }
    }

    //SETUP
    public void SetText(string value)
    {
        gridNumberText.text = value;
    }

    public void ChangeElevation()
    {
        Vector3 adjustedPosition = new Vector3(transform.position.x, transform.position.y + (Globals.TILE_HEIGHT_HALF * elevationLevel), transform.position.z + elevationLevel);
        transform.position = adjustedPosition;
    }

    public void SetColourValues(NodeColours nodeColours)
    {
        moveNodeColour = nodeColours.moveNodeColour;
        print(nodeColours.moveNodeColour);
        selectedMoveNodeColour = nodeColours.selectedMoveNodeColour;
        rangeNodeColour = nodeColours.rangeNodeColour;
        selectedRangeNodeColour = nodeColours.selectedRangeNodeColour;
        blueOccupiedNodeColour = nodeColours.blueOccupiedNodeColour;
        redOccupiedNodeColour = nodeColours.redOccupiedNodeColour;
        noTeamOccupiedNodeColour = nodeColours.noTeamOccupiedNodeColour;
    }

    //INPUT
    public void OnMouseEnter()
    {
        if (IsMoveNode)
        { 
            nodeOverlay.color = selectedMoveNodeColour;

            //show overlay
            nodeOverlay.enabled = true;
        }
        else if (IsRangeNode)
        {
            nodeOverlay.color = selectedRangeNodeColour;

            //show overlay
            nodeOverlay.enabled = true;
        }
        else
        {
            nodeOverlay.enabled = true;
            nodeOverlay.color = Color.white;
        }
    }

    public void OnMouseOver()
    {
        if (inputController.InputMode == InputState.Move)
        {
            if (Input.GetMouseButtonDown(0))
            {
                if (IsMoveNode)
                {
                    Unit unit = GameObject.FindGameObjectWithTag("BattleController").GetComponent<BattleController>().SelectedUnit;
                    unit.StartMovement(this);
                }
            }
        }
    }

    public void OnMouseExit()
    {
        ColourNode();
    }

    //UI
    public void ColourNode()
    {
        //if move node
        if (IsMoveNode)
        {
            nodeOverlay.color = moveNodeColour;
            nodeOverlay.enabled = true;
        }
        else if (IsRangeNode)
        {
            if (IsOccupied)
            {
                switch (Occupant.Team)
                {
                    case TeamColour.Blue:
                        nodeOverlay.color = blueOccupiedNodeColour;
                        break;

                    case TeamColour.Red:
                        nodeOverlay.color = redOccupiedNodeColour;
                        break;

                    case TeamColour.None:
                        nodeOverlay.color = noTeamOccupiedNodeColour;
                        break;

                }
            }
            else
            {
                nodeOverlay.color = rangeNodeColour;
            }

            //show overlay
            nodeOverlay.enabled = true;
        }
        else
        {
            if (IsOccupied)
            {
                switch (Occupant.Team)
                {
                    case TeamColour.Blue:
                        nodeOverlay.color = blueOccupiedNodeColour;
                        break;

                    case TeamColour.Red:
                        nodeOverlay.color = redOccupiedNodeColour;
                        break;

                    case TeamColour.None:
                        nodeOverlay.color = noTeamOccupiedNodeColour;
                        break;

                }
            }
            else
            {
                nodeOverlay.enabled = false;
                nodeOverlay.color = Color.white;
            }
        }
    }

    public void SetNodeColour(Color colour, bool isVisible)
    {
        nodeOverlay.color = colour;
        nodeOverlay.enabled = isVisible;

        print("reset colour");
    }
}
