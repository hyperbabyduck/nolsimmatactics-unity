﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
[SelectionBase]
[RequireComponent(typeof(Node))]

public class NodeEditor : MonoBehaviour
{
    Node node;
    [SerializeField] Text gridText;

    private void Awake()
    {
        node = GetComponent<Node>();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateLabel();
    }

    private void UpdateLabel()
    {
        string labelText = node.GridPos.x + "," + node.GridPos.y;
        gridText.text = labelText;
        gameObject.name = "Node " + labelText;
    }
}
